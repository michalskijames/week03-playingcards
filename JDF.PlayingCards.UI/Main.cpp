// Part 1 Joshua Fachs
// Part 2 James Michalski 

// Playing Cards
// Left a bunch of comments so you could see my thought process through this
// Deleted a bunch that I worked out

#include <iostream>
#include <conio.h>
#include <time.h>

using namespace std; 

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Spades, Clubs, Diamonds, Hearts
};

struct Card
{
	string name;
	Rank Rank;
	Suit Suit;
};


// Prototype for replay function
bool PlayAgain(int playAgain);

// Determine which card is higher
Card HighCard(Card card1, Card card2) 
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	} 
	else
	{
		return card2;
	}
}



// Display cards
void PrintCard(Card card)
{
	// Display Ranks not Numbers
	switch(card.Rank)
	{
	case Two:
		cout << "Two";
		break;
	case 3:
		cout << "Three";
		break;
	case 4:
		cout << "Four";
		break;
	case 5:
		cout << "Five";
		break;
	case 6:
		cout << "Six";
		break;
	case 7:
		cout << "Seven";
		break;
	case 8:
		cout << "Eight";
		break;
	case 9:
		cout << "Nine";
		break;
	case 10:
		cout << "Ten";
		break;
	case 11:
		cout << "Jack";
		break;
	case 12:
		cout << "Queen";
		break;
	case 13:
		cout << "King";
		break;
	case 14:
		cout << "Ace";
		break;
	}

	// Display Suits not Numbers
	switch (card.Suit)
	{
	case 0:
		cout << " of Spades ";
		break;
	case 1:
		cout << " of Clubs ";
		break;
	case 2:
		cout << " of Diamonds ";
		break;
	case 3:
		cout << " of Hearts ";
		break;
	}
	// Formatting
	cout << "\n";
}

int main()
{
	string player1;
	string player2;

	cout << "   Enter Player 1's Name: ";
	cin >> player1;
	cout << "   Enter Player 2's Name: ";
	cin >> player2;

	//todo: me trying to get random cards below

	//string ranks[13] = { "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace" };
	//string suits[4] = { "Spades", "Clubs", "Diamonds", "Hearts" };

	do {
		// Formatting
		cout << "\n";

		srand(time(NULL));
		int rank1 = 2 + rand() % 13;
		int suit1 = 0 + rand() % 4;
		int rank2 = 2 + rand() % 13;
		int suit2 = 0 + rand() % 4;

		Card card1;
		card1.Rank = static_cast<Rank>(rank1);
		card1.Suit = static_cast<Suit>(suit1);
		card1.name = player1;
		//card1.Rank = Queen;
		//card1.Suit = Diamonds;
		cout << "   " << card1.name << " has the: ";
		PrintCard(card1);

		Card card2;
		card2.Rank = static_cast<Rank>(rank2);
		card2.Suit = static_cast<Suit>(suit2);
		card2.name = player2;
		//card2.Rank = Three;
		//card2.Suit = Spades;
		cout << "   " <<  card2.name << " has the: ";
		PrintCard(card2);

		Card highCard = HighCard(card1, card2);
		cout << "\n\t" << highCard.name << " Wins! ";
		cout << "The higher card is the: ";
		PrintCard(highCard); 
		
	

	} while (PlayAgain(1));
	//exiting prompt
	cout << "   Press any key to exit.\n";
	

	(void)_getch();
	return 0;
}

//replay function
bool PlayAgain(int playAgain)
{
	cout << "\n   Would you like to play again? Enter 1 for yes, 2 for no. \n";
	cin >> playAgain;
	if (playAgain == 1) { return true; }
	else { return false; }
}
